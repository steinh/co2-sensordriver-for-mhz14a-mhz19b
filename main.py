from machine import Pin
import mhz14
import time


# Minimal usage of the class MHZ14

# Pin GPIO4 wird genutzt (am nodeMCU: D2)
pin_nr = 4
pin = Pin(pin_nr, Pin.IN)
mh14 = mhz14.MHZ14(pin)

while (True):
    print(mh14.pwm_read())
    time.sleep_ms(1000)