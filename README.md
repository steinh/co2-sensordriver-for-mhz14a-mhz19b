# Micropython Driver for the CO2 Sensors MH-Z14A and MH-Z19B

This driver uses the PWM-Port and Interrupts of the Microcontroller board (testet with nodeMCU / ESP8266)

Hardware-Connection:

| Name of Function | Pin of the ESP / nodeMCU | Pin of the MH-Z14A|
|---|---|---|
|Powersupply 5V (!)| VIN|VIN <br />Pin 1, 15, 17 or 23|
|Ground 0V|GND|GND <br />Pin 2, 3, 12, 16 or 22|
|PWM| any interrupt-capable Pin <br> (D2 / GPIO4)|PWM (Pin 6)|

You need to have both files on your micropython device: the driver (mhz14.py) and your personal main.py (minimal usage example attached).
