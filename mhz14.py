from machine import Pin, ADC
import utime

#
# Connection of sensor MHZ14A:
#  V_in (5V) on Pin 1, 15, 17 or 23
#  GND on Pin 2, 3, 12, 16 or 22
#  Pin on 6 or 26
#
# Pin might be every GPIO of an microcontroller
# being able to read digitally.
# (I personally chose GPIO14 (D4) on my ESP8266
#
#  Minimal example of Usage:
# - This class has to be saved as mhz4.py on micropython-device
# - The following code is needed as main.py to work:
# ------------------------------
# from machine import Pin
# import mhz14
# import time
# # Pin GPIO4 wird genutzt (am nodeMCU: D2)
# pin_nr = 4
# pin = Pin(pin_nr, Pin.IN)
# mh14 = mhz14.MHZ14(pin)
# 
# while (True):
#     print(mh14.pwm_read())
#     time.sleep_ms(1000)
# ------------------------------

class MHZ14:

    def irq_callback_pinchanged(self, p -> Pin):
        pin_state = p.value()
        if (pin_state == 1):
            if (self.measurement_starts_at == 0):
                self.measurement_starts_at = utime.ticks_ms()
            else:
                self.measurement_rising = utime.ticks_ms();
                self.t_h = self.measurement_falling - self.measurement_starts_at
                self.t_l = self.measurement_rising - self.measurement_falling
                self.c_ppm = self.max_value * (self.t_h - 2) / (self.t_h + self.t_l - 4)   
                self.measurement_starts_at = 0
        else:
            self.measurement_falling = utime.ticks_ms()
 

    def __init__(self, pin -> Pin, max_value -> int =5000):
        self.pin = pin
        self.max_value = max_value 
        self.measurement_starts_at = 0
        self.measurement_falling = 0
        self.measurement_rising = 0
        self.c_ppm = 0
        pin.irq(trigger=Pin.IRQ_FALLING | Pin.IRQ_RISING, handler=self.irq_callback_pinchanged)
                
                
    def pwm_read(self):
        return self.c_ppm
 